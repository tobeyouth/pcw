var gulp = require('gulp');
var path = require('path');
var webpack = require("webpack");
var util = require('gulp-util');
var webpackConfig = require('./webpack.config.js');

var devConfig = Object.create(webpackConfig);
devConfig.debug = true;
devConfig.watch = true;
devConfig.devtool = "sourcemap";

var releaseConfig = Object.create(webpackConfig)
releaseConfig.watch = false;
releaseConfig.debug = false;

var devCompiler = webpack(devConfig);

gulp.task('dev',function (cb) {
	devCompiler.watch({
		aggregateTimeout: 500,
    poll: true
	},function (err,status) {
		if (err) {
			throw new util.PluginError('[webpack ERROR]', err);
		}
		util.log("[webpack:build-dev]", status.toString({
      colors: true
    }));
	})
});


gulp.task('default',['dev'])