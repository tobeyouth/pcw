import Dispatcher from 'dispatcher'
import Constants from 'constants/MapConstants'

class Action {
  ENTER (data) {
    Dispatcher.handleViewAction({
      type: Constants.ENTER,
      data: data
    })
  }

  buy (data) {
    Dispatcher.handleViewAction({
      type: Constants.Buy,
      data: data
    })
  }

  filter (filtData) {
    Dispatcher.handleViewAction({
      type: Constants.FILT,
      data: filtData
    })
  }
}

export default new Action()