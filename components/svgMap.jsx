import React from 'react'
import Hammer from 'hammerjs'

let dpr = window.devicePixelRatio || 1

export default class SvgMap extends React.Component {
	constructor (props) {
	  super(props)
    this.attributes = {
      'fill': '#c7e4fd',
      'stroke': '#fff',
      'stroke-width': 0,
      'stroke-linejoin': 'round',
      'fill-opacity': 0.39
    }
    this.stateColor = {}
    this.state = {
      x: 0,
      y: 0,
      imageWidth: 0,
      imageHeight: 0,
      imageLoaded: false,
      mapRendered: false,
      scale: 1,
      minScale: 1,
      maxScale: 5,
      scaleCenter: {
        x: 0,
        y: 0
      }
    }
	}

  componentDidMount () {
    this.wrapper = this.refs['svgMap'].getDOMNode()
    this.loadImage()
    this.bindHammer()
  }

  componentWillUpdate () {
    this.wrapper.className = ''
  }

  componentDidUpdate () {
    if (this.state.imageLoaded && !this.state.mapRendered) {
      this.renderPaper()
      this.renderImage()
      this.renderMap()
    }
  }

	render () {
	  let config = this.props.config
    if (!config) {
      return null
    }

    let options = {
      recognizers:{
        tap:{
          time:200,
          threshold: 0
        }
      }
    }

    let transformArr = []
    
    let scale = 'scale('+this.state.scale+','+this.state.scale+')'
    let translate = 'translate3d('+this.state.x+'px,'+this.state.y+'px,0px)'

    transformArr.push(scale)
    transformArr.push(translate)

    let transform = transformArr.join(' ');

    let stype = {
      width: this.props.width,
      height: this.props.height,
      WebkitTransform: transform,
      MozTransform: transform,
      transform: transform
    }

    let wrapStyle = {
      width: this.props.width,
      height: this.props.height + 4,
      overflow: 'auto'
    }

    return (
      <div className="mapBox">
        <div className="svgMap-wrap" style={ wrapStyle }>
          <div id="svgMap" className="svg-map" style={ stype } ref="svgMap"></div>
        </div>
      </div>
    )
	}

  bindHammer () {
    this.hammer = new Hammer.Manager(this.wrapper)
    this.hammer.add(new Hammer.Pinch({ threshold: 0 }))
    this.hammer.add(new Hammer.Pan({ threshold: 0, pointers: 0 }))
    this.hammer.on("pinchmove", this.onPinch.bind(this))
    this.hammer.on("panmove", this.onPan.bind(this))
  }

  onPinch (e) {
    let initScale = this.state.scale
    let scale = initScale * e.scale
    let that = this

    if (that.pinchTimer) {
      return false
    }

    if (scale > this.state.maxScale) {
      scale = this.state.maxScale
    }

    if (scale < this.state.minScale) {
      scale = this.state.minScale
    }

    this.pinchTimer = setTimeout(function () {
      that.setState({
        scale: scale
      })
      clearTimeout(that.pinchTimer)
      that.pinchTimer = null
    },10)

  }

  onPan (e) {
    let that = this

    if (that.panTimer) {
      return false
    }

    let width = this.props.width
    let height = this.props.height
    let x = this.state.x
    let y = this.state.y
    let eX = e.deltaX / this.state.scale / dpr / 2
    let eY = e.deltaY / this.state.scale / dpr / 2
    let xBount = (width * this.state.scale - width) / (this.state.scale * 2)
    let yBount = (height * this.state.scale - height) / (this.state.scale * 2)

    x += eX
    y += eY

    if (x > xBount) {
      x = xBount
    }
    if (x < 0 - xBount) {
      x = 0 - xBount
    }
    if (y > yBount) {
      y = yBount
    }
    if (y < 0 - yBount) {
      y = 0 - yBount
    }

    that.panTimer = setTimeout(function () {
      that.setState({
        x: x,
        y: y
      })
      clearTimeout(that.panTimer)
      that.panTimer = null
    },10)
  }

  renderPaper () {
    let width = this.props.width
    let height = this.props.height
    let wrapper = this.wrapper
    if (!this.paper) {
      this.paper = Raphael(wrapper,width,height)
    }
    this.paper.setSize(width,height)
    this.paper.clear()
  }

  renderImage () {
    let image = this.props.image
    let riot = this.state.riot
    let width = this.state.imageWidth * riot
    let height = this.state.imageHeight * riot
    
    if (this.state.imageLoaded) {
      let transformScale = 's'+this.state.scale+','+this.state.scale+',0,0'
      this.image = this.paper.image(image,0,0,width,height)
      this.image.transform(transformScale)
    }
  }

  renderMap () {
    let paper = this.paper
    let config = this.props.config
    for (let state in config.shapes) {
      let shape = this.scaleShape(config.shapes[state])

      let tra = config['tra'][state]

      let path = paper.path(shape)
      path.id = state
      path.name = config['names'][state]

      path.attr(this.attributes)

      if (tra) {
        path.tra = tra.split(',')
        path.martix.rotate(path.tra[0],path.tra[1],path.tra[2])
        path.transform(path.matrix.toTransformString());
      }

      path.click(this.changeSeat.bind(this,state,config['names'][state],tra))
    }

    this.setState({
      mapRendered: true
    })
  }

  loadImage () {
    let image = new Image()
    let that = this
    image.src = this.props.image

    image.onload = function () {
      that.setState({
        imageWidth: this.width,
        imageHeight: this.height,
        riot: that.props.width / this.width,
        imageLoaded: true
      })
    }
  }

  renderScaleBar () {
    return (
      <div className="scale-bar">
        <a className="enlarge" onClick={ this.enlarge.bind(this,null) }> + </a>
        <a className="reduce" onClick={ this.reduce.bind(this,null) }> - </a>
      </div>
    )
  }

  // tapHandler (e) {
  //   let that = this
  //   if (!this.tapTimer) {
  //     this.tapTimer = setTimeout(function () {
  //       clearTimeout(that.tapTimer)
  //       that.tapTimer = null
  //       that.enlarge(e.center)
  //     },100)
  //   }
  // }

  // doubelTaphandler () {
  //   if (this.tapTimer) {
  //     this.resetScale()
  //     clearTimeout(this.tapTimer)
  //     this.tapTimer = null
  //   }
  // }

  // enlarge (center) {
  //   let scale = this.state.scale * 1.5
    
  //   if (scale > this.state.maxScale) {
  //     scale = this.state.maxScale
  //   }

  //   this.setState({
  //     scale: scale,
  //     scaleCenter: center ? center : this.state.scaleCenter
  //   })

  // }

  // reduce (center) {
  //   let scale = this.state.scale / 1.5
  //   if (scale < this.state.minScale) {
  //     scale = this.state.minScale
  //   }

  //   this.setState({
  //     scale: scale,
  //     scaleCenter: center ? center : this.state.scaleCenter
  //   })
  // }

  // resetScale () {
  //   this.setState({
  //     scale: 1
  //   })
  // }

  changeSeat (id,name,tra) {
    if (!this.props.changeHandler) {
      return false;
    }

    this.props.changeHandler(id,name,tra)
  }

  scaleShape (shape) {
    let reg = /([\W\w,-])(\d+(\.\d+)?)/gi
    let scale = this.state.scale
    let riot = this.state.riot
    let result = shape.replace(reg,function (a,b,c) {
      return b + parseFloat(c,10) * scale * riot
    })
    return result
  }
}
