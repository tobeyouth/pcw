
import './seatMap.scss'
import React from 'react'
import MapAction from 'actions/MapAction'
import MapStore from 'stores/MapStore'
import SvgSeat from './svgSeat'
import SelectedList from './selected-list'
import Hammer from 'react-hammerjs'

export default class SeatMap extends React.Component {
	constructor (props) {
		super(props)
		this.state = {
      width: window.screen.availWidth,
      height: window.screen.availHeight,
      rended: false,
      riot: this.props.config.data.cols / this.props.config.data.rows,
      selecteds: []
    }
	}

  componentDidMount () {
    let map = this.refs['map'].getDOMNode()
    let width = map.offsetWidth
    let height = width * this.state.riot
    this.setState({
      width: width ? width : this.state.width,
      height: height ? height : this.state.height,
      rended: true
    })
  }

	render () {
    return (
      <div className="app">
        { this.renderTitle() }
        { this.renderDesc() }
        <div className="map" ref="map">
          {this.renderSvgSeat()}
        </div>
        { this.renderSelectedList() }
        { this.renderButton() }
      </div>
    )
	}

  renderTitle () {
    let title = this.props.title
    if (title) {
      return (
        <div className="hd">
          { title }
        </div>
      )
    } else {
      return null
    }
  }

  renderDesc () {
    let desc = this.props.desc
    if (!desc) {
      return null
    }

    return (
      <div className="desc">{desc}</div>
    )
  }

	renderSvgSeat () {
    let config = this.props.config
    if (!config || !this.state.rended) {
      return null
    }
    return (
      <SvgSeat
        selecteds = { this.state.selecteds }
        selectHandler = { this.selectedSeat.bind(this) }
        config = { config }
        width = {this.state.width}
        height = {this.state.height} />
    )
	}

  renderSelectedList () {
    if (this.state.selecteds.length) {
      return (
        <SelectedList 
          deleteHandler={this.selectedSeat.bind(this)} 
          selecteds={this.state.selecteds} />
      )
    } else {
      return null
    }
  }

  selectedSeat (seat) {
    if (seat.status === 'saling') {
      let _selecteds = this.state.selecteds
      let _isSelected = false

      _selecteds.map( (item,index) => {
        if (item.id === seat.id) {
          _isSelected = true
          _selecteds.splice(index,1)
        }
      })

      if (!_isSelected) {
        _selecteds.push(seat)
      }

      this.setState({
        selecteds: _selecteds
      })
    }
  }

  renderButton () {
    let _selecteds = this.state.selecteds
    if (!_selecteds || _selecteds.length === 0) {
      return null
    }

    return (
      <div className="button-box">
        <Hammer onTap={ this.submit.bind(this) }>
          <button className="submit" on>提交订单</button>
        </Hammer>
      </div>
    )
  }

  submit () {
    alert('提交的票id为:'+this.state.selecteds.map( (item,index) => {return item.id}).join(','))
  }

}