
import './svgSeat.scss'
import React from 'react'
import Hammer from 'hammerjs'

let dpr = window.devicePixelRatio || 1

const padding = 10
const STATUS = [
  {
    'id': 'saling',
    'name': '销售中'
  },
  {
    'id': 'saleout',
    'name': '已卖光'
  },
  {
    'id': 'locked',
    'name': '已锁定'
  },
  {
    'id': 'selected',
    'name': '已选中'
  }
]
const ATTRS = {
  seat: {
    'fill' : '#ccc',
    'stroke': '#eee',
    'stroke-width': 1,
    'stroke-linejoin': 'round'
  },
  saling : {
    'fill' : '#8bc9fc',
    'stroke': '#eee',
    'stroke-width': 1,
    'stroke-linejoin': 'round'
  },
  saleout: {
    'fill' : '#c8c8c8',
    'stroke': '#eee',
    'stroke-width': 1,
    'stroke-linejoin': 'round'
  },
  locked : {
    'fill' : '#f18983',
    'stroke': '#eee',
    'stroke-width': 1,
    'stroke-linejoin': 'round'
  },
  blank : {
    'stroke': null,
    'stroke-width': 1,
    'stroke-linejoin': 'round'
  },
  selected : {
    'fill': '#bfd834',
    'stroke': '#eee',
    'stroke-width': 1,
    'stroke-linejoin': 'round'
  }
}

export default class SvgSeat extends React.Component {
  constructor (props) {
    super(props)
    let width = this.props.width
    let height = this.props.height
    let rows = this.props.config.data.rows
    let cols = this.props.config.data.cols
    let seatWidth = width / rows - padding * 2
    let seatHeight = seatWidth
    let mapHeight = cols * (seatHeight + padding * 2)

    this.state = {
      choosed: [],
      x: 0,
      y: 0,
      scale: 1,
      minScale: 1,
      maxScale: Math.floor(Math.max(rows,cols) / 5),
      height: mapHeight,
      width: width,
      seatWidth: seatWidth,
      seatHeight: seatHeight
    }
  }

  componentDidMount () {
    this.wrapper = this.refs['svgMap'].getDOMNode()
    this.renderPaper()
    this.renderMap()
    this.bindHammer()
  }

  // componentDidUpdate () {
  //   // this.renderPaper()
  //   // this.renderMap()
  // }

  render () {
    let options = {
      recognizers:{
        tap:{
          time:200,
        }
      }
    }
    
    let transformArr = []
    
    let scale = 'scale('+this.state.scale+','+this.state.scale+')'
    let translate = 'translate3d('+this.state.x+'px,'+this.state.y+'px,0px)'

    transformArr.push(scale)
    transformArr.push(translate)

    let transform = transformArr.join(' ');

    let wrapperStyle = {
      width: this.state.width,
      height: this.state.height,
      overflow: 'hidden',

    }

    let style = {
      width: this.state.width,
      height: this.state.height,
      WebkitTransform: transform,
      MozTransform: transform,
      transform: transform
    }

    return (
      <div className="mapBox">
        <div className="svg-map-wrapper" style={wrapperStyle}>
          <div id="svgMap" className="svg-map" style={ style } ref="svgMap"></div>
        </div>
        { this.renderDesc() }
      </div>
    )
  }

  renderPaper () {
    let width = this.state.width
    let height = this.state.height
    let wrapper = this.wrapper
    if (!this.paper) {
      this.paper = Raphael(wrapper,width,height)
    }
    this.paper.setSize(width,height)
    this.paper.clear()
  }

  renderMap () {
    let seatInfo = this.props.config.data.seat_info
    let width = this.state.width
    let height = this.state.height
    let seatWidth = this.state.seatWidth * this.state.scale
    let seatHeight = this.state.seatHeight * this.state.scale
    let paper = this.paper

    seatInfo.map( (row,index) => {
      row.map( (seat,i) => {
        let shapeArr = []
        let x = (seatWidth + padding * 2) * i + padding
        let y = (seatHeight + padding * 2) * index
        let M = 'M'+ x + ',' + y
        let L1 = 'L' + (x + seatWidth) + ',' + y
        let L2 = 'L' + (x + seatWidth) + ',' + (y + seatHeight)
        let L3 = 'L' + x + ',' + (y + seatHeight)
        let L4 = 'L' + x + ',' + y
        shapeArr.push(M,L1,L2,L3,L4)

        let shape = shapeArr.join('-')
        let path = paper.path(shape)

        let type = seat.type
        let attr = ATTRS[type]
        let selected = false
        if (type === 'seat' && seat.status) {
          attr = ATTRS[seat.status]
        }
        this.props.selecteds.map( (select) => {
          if (select.id === seat.id) {
            attr = ATTRS['selected']
            selected = true
          }
        })
        
        path.attr(attr)

        path.touchend(this.selectSeat.bind(this,seat))
      })
    })
  }

  bindHammer () {
    this.hammer = new Hammer.Manager(this.wrapper)
    this.hammer.add(new Hammer.Pinch({ threshold: 0 }))
    this.hammer.add(new Hammer.Pan({ threshold: 0, pointers: 0 }))
    this.hammer.on("pinchmove", this.onPinch.bind(this))
    this.hammer.on("panmove", this.onPan.bind(this))
  }

  onPinch (e) {
    let initScale = this.state.scale
    let scale = initScale * e.scale
    let that = this

    if (that.pinchTimer) {
      return false
    }

    if (scale > this.state.maxScale) {
      scale = this.state.maxScale
    }

    if (scale < this.state.minScale) {
      scale = this.state.minScale
    }

    this.pinchTimer = setTimeout(function () {
      that.setState({
        scale: scale
      })
      clearTimeout(that.pinchTimer)
      that.pinchTimer = null
    },10)

  }

  onPan (e) {
    let that = this

    if (that.panTimer) {
      return false
    }

    let width = this.props.width
    let height = this.props.height
    let x = this.state.x
    let y = this.state.y
    let eX = e.deltaX / this.state.scale / dpr / 2
    let eY = e.deltaY / this.state.scale / dpr / 2
    let xBount = (width * this.state.scale - width) / (this.state.scale * 2)
    let yBount = (height * this.state.scale - height) / (this.state.scale * 2)

    x += eX
    y += eY

    if (x > xBount) {
      x = xBount
    }
    if (x < 0 - xBount) {
      x = 0 - xBount
    }
    if (y > yBount) {
      y = yBount
    }
    if (y < 0 - yBount) {
      y = 0 - yBount
    }

    that.panTimer = setTimeout(function () {
      that.setState({
        x: x,
        y: y
      })
      clearTimeout(that.panTimer)
      that.panTimer = null
    },10)
  }

  // renderScaleBar () {
  //   return (
  //     <div className="scale-bar">
  //       <a className="enlarge" onClick={ this.enlarge.bind(this,null) }> + </a>
  //       <a className="reduce" onClick={ this.reduce.bind(this,null) }> - </a>
  //     </div>
  //   )
  // }

  // tapHandler (e) {
  //   let that = this
  //   if (!this.tapTimer) {
  //     this.tapTimer = setTimeout(function () {
  //       clearTimeout(that.tapTimer)
  //       that.tapTimer = null
  //       that.enlarge(e.center)
  //     },100)
  //   }
  // }

  // doubelTaphandler () {
  //   if (this.tapTimer) {
  //     this.resetScale()
  //     clearTimeout(this.tapTimer)
  //     this.tapTimer = null
  //   }
  // }

  // enlarge (center) {
  //   let scale = this.state.scale * 1.5

  //   if (scale > this.state.maxScale) {
  //     scale = this.state.maxScale
  //   }

  //   // this.paper.clear()
  //   this.setState({
  //     scale: scale,
  //     scaleCenter: center ? center : this.state.scaleCenter
  //   })
  // }

  // reduce (center) {
  //   let scale = this.state.scale / 1.5
  //   if (scale < this.state.minScale) {
  //     scale = this.state.minScale
  //   }

  //   this.setState({
  //     scale: scale,
  //     scaleCenter: center ? center : this.state.scaleCenter
  //   })
  // }

  // resetScale () {
  //   this.setState({
  //     scale: 1
  //   })
  // }

  scaleShape (shape) {
    let reg = /([\W\w,-])(\d+(\.\d+)?)/gi
    let result = shape.replace(reg,function (a,b,c) {
      return b + parseFloat(c,10)
    })
    return result
  }

  selectSeat (seat,e) {
    e.preventDefault()
    e.stopPropagation()
    if (this.props.selectHandler && typeof(this.props.selectHandler) === 'function') {
      this.props.selectHandler(seat)
    }
  }

  renderDesc () {
    return (
      <div className="seat-desc">
        {
          STATUS.map( (state,index) => {
            let id = state.id
            let name = state.name
            let fill = ATTRS[id].fill
            let itemStyle = {
              color: fill
            }
            let colorStyle = {
              backgroundColor: fill
            }
            return (
              <div className="desc-item" style={itemStyle} key={'_desc_'+index}>
                <span style={colorStyle} className="color"></span>
                <p>{name}</p>
              </div>
            )
          })
        }
      </div>
    )
  }
}