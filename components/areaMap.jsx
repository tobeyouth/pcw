import './areaMap.scss'

import React from 'react'
import SvgMap from './svgMap'

const RIOT = 600 / 700

export default class areaMap extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      width: window.screen.availWidth,
      height: window.screen.availHeight,
      rended: false
    }
  }
    
  componentDidMount () {
    let map = this.refs['map'].getDOMNode()
    let width = map.offsetWidth
    let height = width * RIOT
    this.setState({
      width: width ? width : this.state.width,
      height: height ? height : this.state.height,
      rended: true
    })
  }

  render () {
    return (
      <div className="app">
        { this.renderTitle() }
        { this.renderDesc() }
        <div className="map" ref="map">
         { this.renderMap() }
        </div>
        <div className="tips">小贴士：请点击区域选择座位</div>
      </div>
    )
  }
  
  renderTitle () {
    let title = this.props.title
    if (title) {
      return (
        <div className="hd">
          { title }
        </div>
      )
    } else {
      return null
    }
  }


  renderDesc () {
    let desc = this.props.desc
    if (!desc) {
      return null
    }

    return (
      <div className="desc">{desc}</div>
    )
  }


  renderMap () {
    let config = this.props.config
    if (config) {
      return (
        <SvgMap 
          config={ config }
          width={ this.state.width }
          height={ this.state.height }
          image={ this.props.image }
          changeHandler={ this.choseArea.bind(this) }/>
      ) 
    } else {
      return null
    }
  }

  choseArea (id,name,tra) {
    let href = '/ticket.do?cid='+this.props.cid+'&zome='+encodeURI(id+":"+name);
    alert(href)
    // window.location.href = href
  }
}
