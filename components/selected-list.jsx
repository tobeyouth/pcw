import './selected-list.scss'
import React from 'react'
import Hammer from 'react-hammerjs'

export default class SelectedList extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    if (!this.props.selecteds || this.props.selecteds.length === 0) {
      return null
    }
    let price = 0
    this.props.selecteds.map( (item,index) => {
      if (item.price) {
        price += item.price
      }
    })

    return (
      <div className="selected-list">
        <div className="items">
          {
            this.props.selecteds.map( (seat,index) => {
              return (
                <div className="item" key={'_selected_'+seat.id}>
                  <span className="title">{seat.name}</span>
                  <span className="price">{seat.price}</span>
                  <Hammer  className="delete" onTap={this.tapHandler.bind(this,seat)}>
                    <em>X</em>
                  </Hammer> 
                </div>
              )           
            })
          }
        </div>
        <div className="selected-desc">
          <span className="title">数量：{ this.props.selecteds.length }</span>
          <span className="prices">总价：{ price }</span>
        </div>
      </div>
    )
  }

  tapHandler (seat) {
    if (this.props.deleteHandler && typeof(this.props.deleteHandler) === 'function') {
      this.props.deleteHandler(seat)
    }
  }
}