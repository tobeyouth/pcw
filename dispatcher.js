import React from 'react'
import assign from 'react/lib/Object.assign'
import Dispatcher from 'flux/lib/Dispatcher'

let disp = assign(new Dispatcher(),{
  handleViewAction: function (action) {
    let payload = {
      source: 'VIEW',
      action: action
    }
    this.dispatch(payload)
  },
  handleServeAction: function (action) {
    let payload = {
      source: 'SERVE',
      action: action
    } 
    this.dispatch(payload)
  }
})

export default disp