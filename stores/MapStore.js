import Dispatcher from 'dispatcher'
import eventemitter3 from "eventemitter3"
import assign from 'react/lib/Object.assign'
import Constants from 'constants/MapConstants'

let MapStore = assign(new eventemitter3(),{
	'emitChange': function() {
    this.emit('change');
  },
  'addChangeListener': function(cb,context) {
    this.on('change', cb, context);
  },
  'removeChangeListener': function(cb,context) {
    this.off('change', cb, context);
  },
  'getData': function() {
    // return DetailModel.getData();
  }
})

MapStore.dispatchToken = Dispatcher.register(function (payload) {
	let action = payload.action
	let source = payload.source

	switch (action.type) {
		case Constants.Buy:

			console.log('BUY')
			break

		case Constants.FILT:
			console.log('FILT')
			break
	}
})


export default MapStore