/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(91);


/***/ },

/***/ 91:
/***/ function(module, exports) {

	'use strict';
	
	var doc = window.document;
	var docEl = doc.documentElement;
	var viewport = doc.querySelector('meta[name="viewpoat"]');
	
	var dpr = undefined,
	    scale = undefined,
	    timer = undefined;
	
	var isAndroid = navigator.appVersion.match(/android/gi);
	var isIOS = navigator.appVersion.match(/iphone|ipod|ipad/gi) && !isAndroid;
	
	var DESIGN_WIDTH = 700;
	
	if (isIOS) {
	  if (window.devicePixelRatio >= 3) {
	    dpr = 3;
	  } else if (window.devicePixelRatio >= 2) {
	    dpr = 2;
	  } else {
	    dpr = 1;
	  }
	} else {
	  dpr = 1;
	}
	
	scale = 1 / dpr;
	
	docEl.setAttribute('data-dpr', dpr);
	if (!viewport) {
	  viewport = doc.createElement('meta');
	  viewport.setAttribute('name', 'viewport');
	  viewport.setAttribute('content', 'initial-scale=' + scale + ', maximum-scale=' + scale + ', minimum-scale=' + scale + ', user-scalable=no');
	  docEl.firstElementChild.appendChild(viewport);
	}
	
	var refreshRem = function refreshRem() {
	  var width = docEl.getBoundingClientRect().width;
	  if (width / dpr > DESIGN_WIDTH) {
	    width = DESIGN_WIDTH * dpr;
	  }
	  var rem = width / 10;
	  docEl.style.fontSize = rem + 'px';
	};
	
	window.addEventListener('resize', function () {
	  clearTimeout(timer);
	  timer = setTimeout(refreshRem, 300);
	}, false);
	window.addEventListener('pageshow', function (e) {
	  if (e.persisted) {
	    clearTimeout(timer);
	    timer = setTimeout(refreshRem, 300);
	  }
	}, false);
	
	if (doc.readyState === 'complete') {
	  doc.body.style.fontSize = 12 * dpr + 'px';
	} else {
	  doc.addEventListener('DOMContentLoaded', function (e) {
	    doc.body.style.fontSize = 12 * dpr + 'px';
	  }, false);
	}
	
	refreshRem();

/***/ }

/******/ });
//# sourceMappingURL=vendor.js.map