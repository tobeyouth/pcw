var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

var vendor = ['./vendor/retina.js'];
var cssLoader = [
  'css',
  'autoprefixer?browsers=last 4 version'
];


module.exports = {
  cache: true,
  entry: {
    // area: path.resolve(__dirname, './area.js'),
    seat: path.resolve(__dirname, './seat.js'),
    vendor: vendor
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    chunkFilename: "[id].js"
  },
  noParse: vendor,
  module: {
    loaders: [
      {
        test: /\.css/,
        loader: ExtractTextPlugin.extract("style", cssLoader.join('!'))
      },
      {
        test: /\.scss/,
        loader: ExtractTextPlugin.extract("style", cssLoader.concat('sass').join('!'))
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel'
      },
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        loader: 'babel'
      }
    ],
  },
  resolve: {
    extensions: [
      '',
      '.coffee',
      '.js',
      '.jsx',
      '.webpack.js',
      '.web.js'
    ],
    modulesDirectories: [
      'node_modules',
      './'
    ]
  },
  plugins: [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
    // new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.js'),
    new ExtractTextPlugin("[name].css")
  ]

}
