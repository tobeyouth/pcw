import React from 'react'
import SeatMap from 'components/seatMap'

let config = window.CONFIG

React.render(<SeatMap title={config.title} desc={config.desc} config={config} />,document.getElementById('map'))