import React from 'react'
import AreaMap from 'components/areaMap'

let CONFIG = window.CONFIG

React.render(
    <AreaMap title={CONFIG.title} desc={CONFIG.desc} cid={ CONFIG.cid } image={ CONFIG.image } config={ CONFIG.mapConfig } />,
    document.getElementById('map')
)
